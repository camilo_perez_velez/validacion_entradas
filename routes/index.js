var express = require('express');
var router = express.Router();
var messages = []
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('script', { title: 'Express' });
});

router.get('/message', function(req, res, next) {
  res.render('message', { title: 'Express' });
});

router.post('/message', function(req, res) {
  console.log(req.body);
  messages.push(`${req.body.email}: ${req.body.message}`);
  res.end('OK')
});

router.get('/messages', function(req, res) {
  res.json(messages)
});

module.exports = router;
