function send(e){
    var message = document.getElementById('message').value;
    var specials=/[*|\":<>[\]{}`\\()';@&$]/;
    if (specials.test(message)) { 
        alert("Caracteres no permitidos")
        return;
     }

    $.ajax({
        type: "POST",
        url: '/message',
        data: {
            message: message,
            email: document.getElementById('email').value,
        },
        success: success,
    });
}

function init(){
    var successMessages = function(messages){
        messages.forEach(function(message){
            $('#messages').append(message+'<br/>');
        });
    }
    $.ajax({
        type: "GET",
        url: '/messages',
        success: successMessages,
    });
}

function success(data){
    alert(data);
}

init()